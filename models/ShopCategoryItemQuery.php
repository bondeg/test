<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ShopCategoryItem]].
 *
 * @see ShopCategoryItem
 */
class ShopCategoryItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ShopCategoryItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ShopCategoryItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
