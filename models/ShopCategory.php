<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ShopCategory".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ShopCategoryItem[] $shopCategoryItems
 * @property ShopItem[] $items
 * @property ShopCategoryTag[] $shopCategoryTags
 * @property ShopTag[] $tags
 */
class ShopCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShopCategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategoryItems()
    {
        return $this->hasMany(ShopCategoryItem::className(), ['categoryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(ShopItem::className(), ['id' => 'itemId'])->viaTable('ShopCategoryItem', ['categoryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategoryTags()
    {
        return $this->hasMany(ShopCategoryTag::className(), ['categoryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ShopTag::className(), ['id' => 'tagId'])->viaTable('ShopCategoryTag', ['categoryId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ShopCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopCategoryQuery(get_called_class());
    }
}
