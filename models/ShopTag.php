<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ShopTag".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ShopCategoryTag[] $shopCategoryTags
 * @property ShopCategory[] $categories
 */
class ShopTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShopTag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategoryTags()
    {
        return $this->hasMany(ShopCategoryTag::className(), ['tagId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ShopCategory::className(), ['id' => 'categoryId'])->viaTable('ShopCategoryTag', ['tagId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ShopTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopTagQuery(get_called_class());
    }
}
