<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ShopCategoryItem".
 *
 * @property integer $categoryId
 * @property integer $itemId
 *
 * @property ShopCategory $category
 * @property ShopItem $item
 */
class ShopCategoryItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShopCategoryItem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'itemId'], 'required'],
            [['categoryId', 'itemId'], 'integer'],
            [['categoryId', 'itemId'], 'unique', 'targetAttribute' => ['categoryId', 'itemId'], 'message' => 'The combination of Идентификатор категории and Идентификатор товара has already been taken.'],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['categoryId' => 'id']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => ShopItem::className(), 'targetAttribute' => ['itemId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Идентификатор категории',
            'itemId' => 'Идентификатор товара',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(ShopItem::className(), ['id' => 'itemId']);
    }

    /**
     * @inheritdoc
     * @return ShopCategoryItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopCategoryItemQuery(get_called_class());
    }
}
