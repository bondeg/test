<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ShopItem".
 *
 * @property integer $id
 * @property string $name
 * @property integer $added
 * @property string $description
 *
 * @property ShopCategoryItem[] $shopCategoryItems
 * @property ShopCategory[] $categories
 */
class ShopItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShopItem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'added', 'description'], 'required'],
            [['added'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
            'added' => 'Время добавления',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShopCategoryItems()
    {
        return $this->hasMany(ShopCategoryItem::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(ShopCategory::className(), ['id' => 'categoryId'])->viaTable('ShopCategoryItem', ['itemId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ShopItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopItemQuery(get_called_class());
    }
}
