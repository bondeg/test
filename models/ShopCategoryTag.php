<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ShopCategoryTag".
 *
 * @property integer $categoryId
 * @property integer $tagId
 *
 * @property ShopTag $tag
 * @property ShopCategory $category
 */
class ShopCategoryTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ShopCategoryTag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'tagId'], 'required'],
            [['categoryId', 'tagId'], 'integer'],
            [['categoryId', 'tagId'], 'unique', 'targetAttribute' => ['categoryId', 'tagId'], 'message' => 'The combination of Идентификатор категории and Идентификатор тега has already been taken.'],
            [['tagId'], 'exist', 'skipOnError' => true, 'targetClass' => ShopTag::className(), 'targetAttribute' => ['tagId' => 'id']],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Идентификатор категории',
            'tagId' => 'Идентификатор тега',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(ShopTag::className(), ['id' => 'tagId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'categoryId']);
    }

    /**
     * @inheritdoc
     * @return ShopCategoryTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShopCategoryTagQuery(get_called_class());
    }
}
