<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ShopCategoryItem;

/**
 * ShopCategoryItemSearch represents the model behind the search form about `app\models\ShopCategoryItem`.
 */
class ShopCategoryItemSearch extends ShopCategoryItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'itemId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShopCategoryItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'categoryId' => $this->categoryId,
            'itemId' => $this->itemId,
        ]);

        return $dataProvider;
    }
}
