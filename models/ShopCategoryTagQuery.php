<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ShopCategoryTag]].
 *
 * @see ShopCategoryTag
 */
class ShopCategoryTagQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ShopCategoryTag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ShopCategoryTag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
