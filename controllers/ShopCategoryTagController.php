<?php

namespace app\controllers;

use app\models\ShopCategory;
use app\models\ShopTag;
use Yii;
use app\models\ShopCategoryTag;
use app\models\ShopCategoryTagSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopCategoryTagController implements the CRUD actions for ShopCategoryTag model.
 */
class ShopCategoryTagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopCategoryTag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCategoryTagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopCategoryTag model.
     * @param integer $categoryId
     * @param integer $tagId
     * @return mixed
     */
    public function actionView($categoryId, $tagId)
    {
        return $this->render('view', [
            'model' => $this->findModel($categoryId, $tagId),
        ]);
    }

    /**
     * Creates a new ShopCategoryTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopCategoryTag();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'categoryId' => $model->categoryId, 'tagId' => $model->tagId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ] + $this->getCategoriesAndTags());
        }
    }

    /**
     * Вернёт список товаров и параметров
     * @return array
     */
    private function getCategoriesAndTags(): array {
        $categoryList = ShopCategory::find()->asArray()->all();
        $tagList = ShopTag::find()->asArray()->all();

        return [
            'categoryList' => ArrayHelper::map($categoryList, 'id', 'name'),
            'tagList' => ArrayHelper::map($tagList, 'id', 'name'),
        ];
    }

    /**
     * Updates an existing ShopCategoryTag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $categoryId
     * @param integer $tagId
     * @return mixed
     */
    public function actionUpdate($categoryId, $tagId)
    {
        $model = $this->findModel($categoryId, $tagId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'categoryId' => $model->categoryId, 'tagId' => $model->tagId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ] + $this->getCategoriesAndTags());
        }
    }

    /**
     * Deletes an existing ShopCategoryTag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $categoryId
     * @param integer $tagId
     * @return mixed
     */
    public function actionDelete($categoryId, $tagId)
    {
        $this->findModel($categoryId, $tagId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopCategoryTag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $categoryId
     * @param integer $tagId
     * @return ShopCategoryTag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($categoryId, $tagId)
    {
        if (($model = ShopCategoryTag::findOne(['categoryId' => $categoryId, 'tagId' => $tagId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
