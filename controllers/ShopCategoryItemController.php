<?php

namespace app\controllers;

use app\models\ShopCategory;
use app\models\ShopItem;
use Yii;
use app\models\ShopCategoryItem;
use app\models\ShopCategoryItemSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShopCategoryItemController implements the CRUD actions for ShopCategoryItem model.
 */
class ShopCategoryItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopCategoryItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCategoryItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShopCategoryItem model.
     * @param integer $categoryId
     * @param integer $itemId
     * @return mixed
     */
    public function actionView($categoryId, $itemId)
    {
        return $this->render('view', [
            'model' => $this->findModel($categoryId, $itemId),
        ]);
    }

    /**
     * Creates a new ShopCategoryItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopCategoryItem();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'categoryId' => $model->categoryId, 'itemId' => $model->itemId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ] + $this->getCategoriesAndItems());
        }
    }

    /**
     * Вернёт список товаров и категорий
     * @return array
     */
    private function getCategoriesAndItems(): array {
        $categoryList = ShopCategory::find()->asArray()->all();
        $itemList = ShopItem::find()->asArray()->all();

        return [
            'categoryList' => ArrayHelper::map($categoryList, 'id', 'name'),
            'itemList' => ArrayHelper::map($itemList, 'id', 'name'),
        ];
    }

    /**
     * Updates an existing ShopCategoryItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $categoryId
     * @param integer $itemId
     * @return mixed
     */
    public function actionUpdate($categoryId, $itemId)
    {
        $model = $this->findModel($categoryId, $itemId);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'categoryId' => $model->categoryId, 'itemId' => $model->itemId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ] + $this->getCategoriesAndItems());
        }
    }

    /**
     * Deletes an existing ShopCategoryItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $categoryId
     * @param integer $itemId
     * @return mixed
     */
    public function actionDelete($categoryId, $itemId)
    {
        $this->findModel($categoryId, $itemId)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopCategoryItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $categoryId
     * @param integer $itemId
     * @return ShopCategoryItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($categoryId, $itemId)
    {
        if (($model = ShopCategoryItem::findOne(['categoryId' => $categoryId, 'itemId' => $itemId])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
