<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ShopTag */

$this->title = 'Create Shop Tag';
$this->params['breadcrumbs'][] = ['label' => 'Shop Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-tag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
