<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ShopCategoryTag */

$this->title = $model->categoryId;
$this->params['breadcrumbs'][] = ['label' => 'Shop Category Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-tag-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'categoryId' => $model->categoryId, 'tagId' => $model->tagId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'categoryId' => $model->categoryId, 'tagId' => $model->tagId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'categoryId',
            'tagId',
        ],
    ]) ?>

</div>
