<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopCategoryTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shop Category Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-tag-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Shop Category Tag', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category.name',
            'categoryId',
            'tag.name',
            'tagId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
