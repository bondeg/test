<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShopCategoryTag */
/* @var $categoryList array */
/* @var $tagList array */

$this->title = 'Update Shop Category Tag: ' . $model->categoryId;
$this->params['breadcrumbs'][] = ['label' => 'Shop Category Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoryId, 'url' => ['view', 'categoryId' => $model->categoryId, 'tagId' => $model->tagId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shop-category-tag-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoryList' => $categoryList,
        'tagList' => $tagList,
    ]) ?>

</div>
