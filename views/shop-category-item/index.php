<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShopCategoryItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shop Category Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Shop Category Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category.name',
            'categoryId',
            'item.name',
            'itemId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
