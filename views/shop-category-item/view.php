<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ShopCategoryItem */

$this->title = $model->categoryId;
$this->params['breadcrumbs'][] = ['label' => 'Shop Category Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'categoryId' => $model->categoryId, 'itemId' => $model->itemId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'categoryId' => $model->categoryId, 'itemId' => $model->itemId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'categoryId',
            'itemId',
        ],
    ]) ?>

</div>
