<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShopCategoryItem */
/* @var $categoryList array */
/* @var $itemList array */

$this->title = 'Update Shop Category Item: ' . $model->categoryId;
$this->params['breadcrumbs'][] = ['label' => 'Shop Category Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoryId, 'url' => ['view', 'categoryId' => $model->categoryId, 'itemId' => $model->itemId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shop-category-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categoryList' => $categoryList,
        'itemList' => $itemList,
    ]) ?>

</div>
