-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 16 2017 г., 10:47
-- Версия сервера: 5.6.22-71.0
-- Версия PHP: 7.0.13-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2basic`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ShopCategory`
--

CREATE TABLE `ShopCategory` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор',
  `name` varchar(32) NOT NULL COMMENT 'Название'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ShopCategory`
--

INSERT INTO `ShopCategory` (`id`, `name`) VALUES
(1, 'Продукты'),
(2, 'Телефоны');

-- --------------------------------------------------------

--
-- Структура таблицы `ShopCategoryItem`
--

CREATE TABLE `ShopCategoryItem` (
  `categoryId` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор категории',
  `itemId` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор товара'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ShopCategoryItem`
--

INSERT INTO `ShopCategoryItem` (`categoryId`, `itemId`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `ShopCategoryTag`
--

CREATE TABLE `ShopCategoryTag` (
  `categoryId` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор категории',
  `tagId` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор тега'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ShopCategoryTag`
--

INSERT INTO `ShopCategoryTag` (`categoryId`, `tagId`) VALUES
(2, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `ShopItem`
--

CREATE TABLE `ShopItem` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор',
  `name` varchar(32) NOT NULL COMMENT 'Название',
  `added` int(10) UNSIGNED NOT NULL COMMENT 'Время добавления',
  `description` text NOT NULL COMMENT 'Описание'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ShopItem`
--

INSERT INTO `ShopItem` (`id`, `name`, `added`, `description`) VALUES
(1, 'Малина', 124214215, 'Очень древняя малина'),
(2, 'Свекла', 4294967295, 'Сделает ваш борщ красным'),
(3, 'Телефон', 1241253252, 'Обычный кнопочный стационарный телефон'),
(4, 'Радиотелефон', 4294967295, 'Стационарный радиотелефон');

-- --------------------------------------------------------

--
-- Структура таблицы `ShopTag`
--

CREATE TABLE `ShopTag` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Идентификатор',
  `name` varchar(32) NOT NULL COMMENT 'Название'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `ShopTag`
--

INSERT INTO `ShopTag` (`id`, `name`) VALUES
(1, 'диагональ'),
(2, 'сорт'),
(3, 'происхождение'),
(4, 'цвет');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ShopCategory`
--
ALTER TABLE `ShopCategory`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ShopCategoryItem`
--
ALTER TABLE `ShopCategoryItem`
  ADD UNIQUE KEY `categoryId` (`categoryId`,`itemId`),
  ADD KEY `itemId` (`itemId`);

--
-- Индексы таблицы `ShopCategoryTag`
--
ALTER TABLE `ShopCategoryTag`
  ADD UNIQUE KEY `categoryId` (`categoryId`,`tagId`),
  ADD KEY `tagId` (`tagId`);

--
-- Индексы таблицы `ShopItem`
--
ALTER TABLE `ShopItem`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ShopTag`
--
ALTER TABLE `ShopTag`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ShopCategory`
--
ALTER TABLE `ShopCategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `ShopItem`
--
ALTER TABLE `ShopItem`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `ShopTag`
--
ALTER TABLE `ShopTag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=5;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ShopCategoryItem`
--
ALTER TABLE `ShopCategoryItem`
  ADD CONSTRAINT `ShopCategoryItem_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `ShopCategory` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ShopCategoryItem_ibfk_2` FOREIGN KEY (`itemId`) REFERENCES `ShopItem` (`id`);

--
-- Ограничения внешнего ключа таблицы `ShopCategoryTag`
--
ALTER TABLE `ShopCategoryTag`
  ADD CONSTRAINT `ShopCategoryTag_ibfk_1` FOREIGN KEY (`tagId`) REFERENCES `ShopTag` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ShopCategoryTag_ibfk_2` FOREIGN KEY (`categoryId`) REFERENCES `ShopCategory` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
